import { Component, OnInit } from '@angular/core';
import { User } from '../IUser';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  user: User = {
    id: 1,
    name: "John",
    age: 22,
    email: "jhonedoe@e.com"
  };

  users: User[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  onSelect(user: User): void {
    this.user = user;
  }

  onClick(): void {
    this.users = [{
      id: 1,
      name: "John",
      age: 22,
      email: "jhonedoe@e.com"
    }, {
      id: 2,
      name: "Marcus",
      age: 21,
      email: "Marcus@e.com"
    }, {
      id: 3,
      name: "Alex",
      age: 25,
      email: "alex@e.com"
    }];
  }
}
